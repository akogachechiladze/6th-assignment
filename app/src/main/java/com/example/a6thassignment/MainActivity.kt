package com.example.a6thassignment

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.a6thassignment.databinding.ActivityMainBinding
import retrofit2.HttpException
import java.io.IOException

const val TAG = "MainActivity"

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var todoAdapter: RecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)

        setContentView(binding.root)
        setupRecyclerView()

        lifecycleScope.launchWhenCreated {
            binding.proggresbar.isVisible = true
            val response = try {
                RetrofitInstance.api.getTodos()

            }catch (e: IOException) {
                Log.e(TAG, "IOException, internet problem", )
                binding.proggresbar.isVisible = false
                return@launchWhenCreated
            }catch (e: HttpException){
                Log.e(TAG, "HttpException, unexpected response", )
                binding.proggresbar.isVisible = false
                return@launchWhenCreated
            }
            if (response.isSuccessful && response.body() != null) {
                todoAdapter.todos = response.body()?.content!!
                binding.proggresbar.isVisible = false
            }else {
                Log.e(TAG, "response not successful", )
            }
            binding.proggresbar.isVisible = false
        }
    }
    private fun setupRecyclerView() = binding.RecyclerView.apply {
        todoAdapter = RecyclerViewAdapter()
        adapter = todoAdapter
        layoutManager = LinearLayoutManager(this@MainActivity)
    }
}